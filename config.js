/**
 * Discord Bot settings. These are mandatory and will affect what the bot
 *   posts about, and where it posts to.
 */

module.exports = {
  discord: {
    // The Discord token of the Bot to post through.
    token: "NDkwOTU3NTMxNTU2ODcyMTky.DontsQ.ps4qOgWLUvw0ZZ9GqDTMqV51zjo",
    // The ID of the discord channel to post battleboard infos to.
    feedChannelId: '493626807493460000',
    // The ID of the discord channel to post albion status infos to.
    statusChannelId: '493626841739952131'
  },
  guild: {
    // The name of your guild (or guilds, if the guild is large).
    guilds: ['The Revival'],
    // The alliance your guild belongs to
    alliance: 'POWER'
  },
  battle: {
    // Min players to report as battle
    minPlayers: 7,
    // Min guild players to report as battle
    minRelevantPlayers: 4
  },
  kill: {
    // Min killfame to report kill
    minFame: 1
  }
};